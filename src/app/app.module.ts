import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { appRoutingProviders, routing } from './app.routing';
import { MomentModule } from 'angular2-moment';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ToastyModule } from 'ng2-toasty';
import { FacebookModule } from 'ngx-facebook';

import { HttpService } from './services/http.service';
import { GlobalService } from './services/global.service';
import { CompanyService } from './services/company.service';
import { DataStoreService } from './services/data-store.service';
import { ModalWindowService } from './services/modal-window.service';
import { LiveChatService } from './services/live-chat.service';
import { ToastService } from './services/toast.service';
import { UserService } from './services/user.service';
import { HttpClientService } from './services/http-client.service';
import { ModalMessageService } from './services/modal-message.service';

import { KeysPipe } from './pipes/keys.pipe';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { StepsComponent } from './components/steps/steps.component';
import { LogosComponent } from './components/logos/logos.component';
import { CompanyRequirementsComponent } from './components/company-requirements/company-requirements.component';
import { MultichoiceQuestionComponent } from './components/multichoice-question/multichoice-question.component';
import { InstantQuoteComponent } from './components/instant-quote/instant-quote.component';
import { RemoveFromQuoteComponent } from './components/remove-from-quote/remove-from-quote.component';
import { ReviewAndPaymentComponent } from './components/review-and-payment/review-and-payment.component';
import { ThankYouComponent } from './components/thank-you/thank-you.component';
import { FooterComponent } from './components/footer/footer.component';
import { LiveChatComponent } from './components/live-chat/live-chat.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { QuoteSidebarComponent } from './components/quote-sidebar/quote-sidebar.component';
import { ModalEmailQuoteComponent } from './components/modal-email-quote/modal-email-quote.component';

import { LivechatDirective } from './directives/livechat.directive';
import { CanLoadPageGuard } from './guards/can-load-page.guard';
import { ModalMessageComponent } from './components/modal-message/modal-message.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactComponent } from './components/contact/contact.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { CookiesAgreementComponent } from './components/cookies-agreement/cookies-agreement.component';
import { TermsOfUseComponent } from './components/terms-of-use/terms-of-use.component';
import { FreeDownloadComponent } from './components/free-download/free-download.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LandingPageComponent,
    CompanyDetailsComponent,
    NotFoundComponent,
    StepsComponent,
    LogosComponent,
    CompanyRequirementsComponent,
    MultichoiceQuestionComponent,
    InstantQuoteComponent,
    RemoveFromQuoteComponent,
    ReviewAndPaymentComponent,
    ThankYouComponent,
    FooterComponent,
    LiveChatComponent,
    KeysPipe,
    ModalWindowComponent,
    QuoteSidebarComponent,
    LivechatDirective,
    ModalEmailQuoteComponent,
    ModalMessageComponent,
    AboutUsComponent,
    ContactComponent,
    TestimonialsComponent,
    PrivacyPolicyComponent,
    CookiesAgreementComponent,
    TermsOfUseComponent,
    FreeDownloadComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    routing,
    MomentModule,
    FlexLayoutModule,
    FormsModule,
    ToastyModule.forRoot(),
    FacebookModule.forRoot()
  ],
  providers: [
    {
      provide: HttpService,
      useFactory: HttpServiceLoader,
      deps: [XHRBackend, RequestOptions]
    },
    appRoutingProviders,
    GlobalService,
    CompanyService,
    DataStoreService,
    ModalWindowService,
    LiveChatService,
    ToastService,
    UserService,
    HttpClientService,
    CanLoadPageGuard,
    ModalMessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpServiceLoader(backend: XHRBackend, options: RequestOptions) {
  return new HttpService(backend, options);
}
