import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CompanyRequirementsComponent } from './components/company-requirements/company-requirements.component';
import { InstantQuoteComponent } from './components/instant-quote/instant-quote.component';
import { ReviewAndPaymentComponent } from './components/review-and-payment/review-and-payment.component';
import { ThankYouComponent } from './components/thank-you/thank-you.component';
import { CanLoadPageGuard } from './guards/can-load-page.guard';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ContactComponent } from './components/contact/contact.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './components/terms-of-use/terms-of-use.component';
import { FreeDownloadComponent } from './components/free-download/free-download.component';


const appRoutes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
    pathMatch: 'full'
  },
  {
    path: 'company/:company_number',
    component: CompanyDetailsComponent,
    pathMatch: 'full'
  },
  {
    path: 'company-requirements',
    component: CompanyRequirementsComponent,
    pathMatch: 'full',
    canActivate: [CanLoadPageGuard]
  },
  {
    path: 'instant-quote',
    component: InstantQuoteComponent,
    pathMatch: 'full',
    canActivate: [CanLoadPageGuard]
  },
  {
    path: 'review-and-payment',
    component: ReviewAndPaymentComponent,
    pathMatch: 'full',
    canActivate: [CanLoadPageGuard]
  },
  {
    path: 'thank-you',
    component: ThankYouComponent,
    pathMatch: 'full',
    canActivate: [CanLoadPageGuard]
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    pathMatch: 'full'
  },
  {
    path: 'contact',
    component: ContactComponent,
    pathMatch: 'full'
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    pathMatch: 'full'
  },
  {
    path: 'terms-of-use',
    component: TermsOfUseComponent,
    pathMatch: 'full'
  },
  {
    path: 'free-download',
    component: FreeDownloadComponent,
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
// export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});
