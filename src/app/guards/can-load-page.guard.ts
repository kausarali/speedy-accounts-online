import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { DataStoreService } from '../services/data-store.service';
import { ToastService } from '../services/toast.service';

@Injectable()
export class CanLoadPageGuard implements CanActivate {

  constructor(
    private toastService: ToastService,
    private dataStore: DataStoreService,
    private router: Router
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    // console.log('CanLoadPageGuard:');
    // console.log('Next:', next);
    // console.log('State:', state);

    // COMPANY REQUIREMENTS
    if (state.url === '/company-requirements') {
      if (!this.companyNumberExists()) {
        this.router.navigate(['/']);
        return false;
      }
      if (this.dataStore.isPaid()) {
        this.router.navigate(['/thank-you']);
        return false;
      }
    }

    // INSTANT QUOTE
    if (state.url === '/instant-quote') {
      if (!this.companyNumberExists() || !this.allQuestionsAnswered()) {
        this.router.navigate(['/']);
        return false;
      }
      if (this.dataStore.isPaid()) {
        this.router.navigate(['/thank-you']);
        return false;
      }
    }

    // REVIEW AND PAYMENT
    if (state.url === '/review-and-payment') {
      if (!this.companyNumberExists() || !this.allQuestionsAnswered()) {
        this.router.navigate(['/']);
        return false;
      }
      if (this.dataStore.isPaid()) {
        this.router.navigate(['/thank-you']);
        return false;
      }
    }

    // THANK YOU
    if (state.url === '/thank-you') {
      if (!this.dataStore.isPaid()) {
        this.router.navigate(['/review-and-payment']);
        return false;
      }
    }

    return true;
  }

  companyNumberExists() {
    if (!this.dataStore.getCompanyNumber()) {
      console.log('Company number doesn\'t exist.');
      this.toastService.error('Company not recognised. Please check you entry and try again.');
      return false;
    }
    return true;
  }

  allQuestionsAnswered() {
    const packageDetails = this.dataStore.getPackageDetails();

    if (packageDetails['actively_trading'].options['dormant'].selected) {
      return true;
    }

    for (const packageKey in packageDetails) {
      if (!packageDetails[packageKey].answered) {
        console.log('Question not answered.');
        this.toastService.error('Please answer all the questions on the previous page before you can move on to the next one.');
        return false;
      }
    }
    return true;
  }
}
