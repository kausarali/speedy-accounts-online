import { TestBed, async, inject } from '@angular/core/testing';

import { CanLoadPageGuard } from './can-load-page.guard';

describe('CanLoadPageGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanLoadPageGuard]
    });
  });

  it('should ...', inject([CanLoadPageGuard], (guard: CanLoadPageGuard) => {
    expect(guard).toBeTruthy();
  }));
});
