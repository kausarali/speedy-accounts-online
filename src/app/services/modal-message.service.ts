import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ModalMessageService {
  private pubSubber: EventEmitter<any> = new EventEmitter();
  private messageModal: any = {
    title: 'Error',
    message: 'An error occured.',
    visible: false
  };

  constructor() {}

  getPubSubber(): EventEmitter<any> {
    return this.pubSubber;
  }

  error(message: string | Array<string>) {

    if (message instanceof Array) {
      this.messageModal.message = '';
      message.forEach(element => {
        if (this.messageModal.message !== '') {
          this.messageModal.message += '\n';
        }
        this.messageModal.message += element;
      });
    } else {
      this.messageModal.message = message;
    }

    this.messageModal.visible = true;
    this.pubSubber.emit(this.messageModal);
  }

  hide() {
    this.messageModal.visible = false;
    this.pubSubber.emit(this.messageModal);
  }
}
