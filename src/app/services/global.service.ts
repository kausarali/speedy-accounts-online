import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ToastService } from './toast.service';
import { ModalMessageService } from './modal-message.service';

@Injectable()
export class GlobalService {
  static APIPath: string = 'https://api.companieshouse.gov.uk/';
  static liveChatAPIPath = 'https://api.livechatinc.com/';
  static SAOAPIPath: string = 'https://speedyaccountsonline.co.uk/api/public/api/';
  static liveChatLicenseId: number = 9061515;
  // static apiKey: string = 'lgU_9uPuiv6qkX0uI-eZkmBnIQ-hKttbwUuoz7Dj';
  token: string;

  constructor(
    private modalMessageService: ModalMessageService,
    private toastService: ToastService
  ) {
    this.handleClientId();
    this.handleToken();
  }

  static extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  static handleError(error: any) {
    const errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg);
    return Observable.throw(errMsg);
  }

  handleResponse(data: any) {
    console.log('handleResponse():');

    if (!data.errors) {

      // if messages
      if (data.messages) {
        data.messages.forEach(element => {
          this.toastService.success(element);
        });
      }
      // if message
      if (data.message) {
        this.toastService.success(data.message);
      }

    } else if (data.errors) {

      // if messages
      if (data.messages) {
        data.messages.forEach(element => {
          // this.toastService.error(element);
          this.modalMessageService.error(element);
        });
      }
      // if message
      if (data.message) {
        // this.toastService.error(data.message);
        this.modalMessageService.error(data.message);
      }

    } else {
      console.log('Failed to check for errors.');
    }

    if (data && data.payload) {
      return data.payload;
    }
  }

  getClientId() {
    return localStorage.getItem('client_id');
  }

  handleClientId() {
    const clientId = localStorage.getItem('client_id');

    if (!clientId) {
      console.log('No client ID!\nGenerating a new one!');
      alert('Failed to start the chat.');
      localStorage.setItem('client_id', this.generateClientId().toString());
    }
  }

  generateClientId() {
    if (!Date.now) {
      Date.now = function () {
        return new Date().getTime();
      };
    }
    return Date.now();
  }

  handleToken() {
    const token = localStorage.getItem('token');
    if (token) {
      this.token = token;
    }
    return token;
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.token = token;
  }
}
