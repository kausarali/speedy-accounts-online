import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { GlobalService } from './global.service';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class CompanyService {

  constructor(private http: HttpService) {}

  public searchCompany(query: string) {
    return this.http.get(`${GlobalService.APIPath}search/companies?q=${query}`)
        .map(GlobalService.extractData)
        .catch(GlobalService.handleError);
  }

  public getProfile(companyNumber: number) {
    return this.http.get(`${GlobalService.APIPath}company/${companyNumber}`)
        .map(GlobalService.extractData)
        .catch(GlobalService.handleError);
  }

  public getPersonsWithSignificantControl(companyNumber: number) {
    return this.http.get(`${GlobalService.APIPath}company/${companyNumber}/persons-with-significant-control`)
        .map(GlobalService.extractData)
        .catch(GlobalService.handleError);
  }

}
