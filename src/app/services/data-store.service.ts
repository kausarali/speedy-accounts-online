import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class DataStoreService {
  private packageDetails: any = {};
  private pubSubber: EventEmitter<any> = new EventEmitter();
  private customerInfo: any = { company: {} };
  private servicesIncluded: Array<string> = [];
  private personsWithSignificantControl = [];
  private companyNumber: number;
  private paid: boolean = false;

  constructor() {
    this.packageDetails = {
      actively_trading: {
        title: 'Is Your Company Actively Trading?',
        answered: false,
        options: {
          active: {
            title: 'My company is active',
            price: 497,
            selected: false,
            icon: 'check',
            servicesIncluded: ['Annual Accounts (Active)', 'Corporation Tax Return (Active)'],
            serviceId: 1
          },
          dormant: {
            title: 'My company is dormant',
            price: 247,
            selected: false,
            icon: 'multiply',
            servicesIncluded: ['Annual Accounts (Dormant)', 'Corporation Tax Return (Dormant)'],
            serviceId: 2
          }
        }
      },
      bookkeeping: {
        title: 'Do You Have Up To Date Bookkeeping Records?',
        answered: false,
        shortTitle: 'Bookkeeping',
        // confirmed: false,
        removed: false,
        options: {
          no: {
            title: 'I have kept records of all of my company transactions',
            price: 0,
            selected: false,
            icon: 'check',
            servicesIncluded: []
          },
          yes: {
            title: 'I need someone to go through my documents and extract the information required',
            price: 300,
            selected: false,
            icon: 'multiply',
            servicesIncluded: ['Bookkeeping'],
            serviceId: 3
          }
        }
      },
      vat: {
        title: 'Is Your Company VAT Registered?',
        answered: false,
        shortTitle: 'VAT Returns',
        // confirmed: false,
        removed: false,
        options: {
          filed: {
            title: 'And I have filed all of my VAT returns',
            price: 0,
            selected: false,
            icon: 'check',
            servicesIncluded: []
          },
          notFiled: {
            title: 'But I have not filed my VAT returns',
            price: 300,
            selected: false,
            icon: 'check',
            servicesIncluded: ['VAT Returns'],
            serviceId: 4
          },
          notRegistered: {
            title: 'My company is not VAT registered',
            price: 0,
            selected: false,
            icon: 'multiply',
            servicesIncluded: []
          }
        }
      },
      employer: {
        title: 'Is Your Company Registered As An Employer?',
        answered: false,
        shortTitle: 'Payroll',
        // confirmed: false,
        removed: false,
        options: {
          filed: {
            title: 'And I have filed all of my payroll returns',
            price: 0,
            selected: false,
            icon: 'check',
            servicesIncluded: []
          },
          notFiled: {
            title: 'But I have not filed my payroll returns',
            price: 300,
            selected: false,
            icon: 'check',
            servicesIncluded: ['Payroll Submissions'],
            serviceId: 5
          },
          notRegistered: {
            title: 'My company is not registered as an employer',
            price: 0,
            selected: false,
            icon: 'multiply',
            servicesIncluded: []
          }
        }
      }
    };
  }

  setPersonsWithSignificantControl(personsWithSignificantControl) {
    this.personsWithSignificantControl = personsWithSignificantControl;
  }

  getPersonsWithSignificantControl() {
    return this.personsWithSignificantControl;
  }

  setCompanyNumber(companyNumber: number) {
    this.companyNumber = companyNumber;
  }

  getCompanyNumber() {
    return this.companyNumber;
  }

  getServicesIncludedIds() {
    const servicesIncludedIds = [];
    // tslint:disable-next-line:forin
    for (const packageKey in this.packageDetails) {
      for (const optionKey in this.packageDetails[packageKey].options) {
        if (this.packageDetails[packageKey].options[optionKey].selected) {
          if ('serviceId' in this.packageDetails[packageKey].options[optionKey]) {
            servicesIncludedIds.push(this.packageDetails[packageKey].options[optionKey].serviceId);
          }
        }
      }
    }
    return servicesIncludedIds;
  }

  updateServicesIncluded() {
    this.servicesIncluded = [];
    for (const packageKey in this.packageDetails) {
      // tslint:disable-next-line:forin
      for (const optionKey in this.packageDetails[packageKey].options) {
        if (this.packageDetails[packageKey].options[optionKey].selected) {
          this.servicesIncluded.push.apply(this.servicesIncluded, this.packageDetails[packageKey].options[optionKey].servicesIncluded);
        }
      }
    }
    // console.log('Services Included:');
    // console.log(this.servicesIncluded);
  }

  getServicesIncluded() {
    return this.servicesIncluded;
  }

  getPackageDetails() {
    return this.packageDetails;
  }

  getTotalPrice() {
    let totalPrice = 0;
    // tslint:disable-next-line:forin
    for (const packageKey in this.packageDetails) {
      // tslint:disable-next-line:forin
      for (const optionKey in this.packageDetails[packageKey].options) {
        if (this.packageDetails[packageKey].options[optionKey].selected) {
          totalPrice += this.packageDetails[packageKey].options[optionKey].price;
        }
      }
    }
    return totalPrice;
  }

  updatePackage(packageKey: string, optionKey: string) {
    // console.log('dataStore: updatePackage():');
    // tslint:disable-next-line:forin
    for (const optKey in this.packageDetails[packageKey].options) {
      this.packageDetails[packageKey].options[optKey].selected = false;
    }
    this.packageDetails[packageKey].options[optionKey].selected = true;
    this.packageDetails[packageKey].answered = true;
    this.updateServicesIncluded();
    this.pubSubber.emit({
      packageDetails: this.packageDetails
    });
  }

  addPackage(packageKey: string) {
    for (const optKey in this.packageDetails[packageKey].options) {
      if (this.packageDetails[packageKey].options[optKey].servicesIncluded.length > 0) {
        this.packageDetails[packageKey].options[optKey].selected = true;
      } else {
        this.packageDetails[packageKey].options[optKey].selected = false;
      }
    }
    console.log('=== SERVICE ADDED ===')
    console.log(this.packageDetails[packageKey]);
    this.updateServicesIncluded();
    this.pubSubber.emit({
      packageDetails: this.packageDetails
    });
  }

  removePackage(packageKey: string) {
    // tslint:disable-next-line:forin
    for (const optionKey in this.packageDetails[packageKey].options) {
      this.packageDetails[packageKey].options[optionKey].selected = false;
    }
    this.packageDetails[packageKey].removed = true;
    this.updateServicesIncluded();
    this.pubSubber.emit({
      packageDetails: this.packageDetails
    });
  }

  getCompanyInfo() {
    return this.customerInfo.company;
  }

  setCompanyInfo(data: any) {
    this.customerInfo.company = data;
  }

  getPubSubber(): EventEmitter<any> {
    return this.pubSubber;
  }

  emitPubSubber(obj: any) {
    this.pubSubber.emit(obj);
  }

  setPaid(isPaid: boolean) {
    this.paid = isPaid;
  }

  isPaid() {
    return this.paid;
  }
}
