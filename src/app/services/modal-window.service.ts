import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ModalWindowService {
  private termsOfServiceModal: any = { visible: false };
  private emailQuoteModal: any = { visible: false };
  private pubSubber: EventEmitter<any> = new EventEmitter();

  constructor() { }

  getPubSubber(): EventEmitter<any> {
    return this.pubSubber;
  }

  emitPubSubber(obj: any) {
    this.pubSubber.emit(obj);
  }

  toggleTermsOfServiceModal(visible: boolean) {
    this.termsOfServiceModal.visible = visible;
    this.pubSubber.emit({
      termsOfServiceModal: this.termsOfServiceModal
    });
  }

  toggleEmailQuoteModal(visible: boolean) {
    this.emailQuoteModal.visible = visible;
    this.pubSubber.emit({
      emailQuoteModal: this.emailQuoteModal
    });
  }
}
