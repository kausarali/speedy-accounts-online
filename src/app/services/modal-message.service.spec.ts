import { TestBed, inject } from '@angular/core/testing';

import { ModalMessageService } from './modal-message.service';

describe('ModalMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalMessageService]
    });
  });

  it('should be created', inject([ModalMessageService], (service: ModalMessageService) => {
    expect(service).toBeTruthy();
  }));
});
