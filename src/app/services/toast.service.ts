import { Injectable } from '@angular/core';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

@Injectable()
export class ToastService {
  toastOptions: ToastOptions;

  constructor(
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
  ) {
    this.toastyConfig.theme = 'default';
    this.toastOptions = {
      title: 'Toast',
      msg: 'Message',
      showClose: true,
      timeout: 5000,
      theme: 'default',
      onAdd: (toast: ToastData) => { },
      onRemove: (toast: ToastData) => { }
    };
  }

  success(message: string, title: string = 'Success!') {
    this.toastOptions.msg = message;
    this.toastOptions.title = title;
    this.toastyService.success(this.toastOptions);
  }

  error(message: string, title: string = 'Error!') {
    this.toastOptions.msg = message;
    this.toastOptions.title = title;
    this.toastyService.error(this.toastOptions);
  }

  warning(message: string, title: string = 'Warning!') {
    this.toastOptions.msg = message;
    this.toastOptions.title = title;
    this.toastyService.warning(this.toastOptions);
  }

  info(message: string, title: string = 'Info!') {
    this.toastOptions.msg = message;
    this.toastOptions.title = title;
    this.toastyService.info(this.toastOptions);
  }

  wait(message: string, title: string = 'Please wait!') {
    this.toastOptions.msg = message;
    this.toastOptions.title = title;
    this.toastyService.wait(this.toastOptions);
  }

  cleaer(id: number) {
    this.toastyService.clear(id);
  }

  clearAll() {
    this.toastyService.clearAll();
  }
}
