import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { GlobalService } from './global.service';
import { ToastService } from './toast.service';

@Injectable()
export class LiveChatService {
  secured_session_id: string;

  constructor(
    private toastService: ToastService,
    private http: Http,
    private globalService: GlobalService
  ) { }

  public generateHeaders(headers: Headers = new Headers()) {
    headers.append('Content-Type', 'application/json');
    headers.append('X-API-Version', '2');
    return headers;
  }

  public startChat(callback: any) {
    if (!this.secured_session_id) {
      const data = {
        licence_id: GlobalService.liveChatLicenseId,
        welcome_message: 'Hi'
      };
      const requestOptions = new RequestOptions();
      requestOptions.headers = this.generateHeaders(new Headers());

      return this.http.post(`${GlobalService.liveChatAPIPath}visitors/${this.globalService.getClientId()}/chat/start`, data, requestOptions)
        .map(GlobalService.extractData)
        .catch(GlobalService.handleError)
        .subscribe(res => {
          console.log('LiveChatService: startChat():');
          console.log(res);
          if (res && res.banned) {
            console.log('LiveChatInc: Banned.');
            alert('LiveChatInc: Banned.');
            callback(false);
          }
          if ('secured_session_id' in res && res.secured_session_id !== '') {
            this.secured_session_id = res.secured_session_id;
            localStorage.setItem('secured_session_id', res.secured_session_id);
            callback(true);
          } else {
            console.log('Failed to get secured_session_id.');
            console.log('Response:');
            console.log(res);
            console.log('Using old one...');
            const oldSecuredSessionId = localStorage.getItem('secured_session_id');
            if (oldSecuredSessionId) {
              this.secured_session_id = oldSecuredSessionId;
              callback(true);
            } else {
              console.log('No old secured_session_id found. Failed to start the chat.');
              callback(false);
            }
          }
        },
        err => {
          this.toastService.error('LiveChatInc Error occured. More info in the console.');
          console.log(err);
        });
    } else {
      console.log('LiveChat Secured Session Id already exists. Doing nothing.');
      callback(true);
    }
  }

  public getMessages(lastMessageId: number = null) {
    const params: URLSearchParams = new URLSearchParams();
    params.set('licence_id', GlobalService.liveChatLicenseId.toString());
    params.set('secured_session_id', this.secured_session_id);
    if (lastMessageId) {
      params.set('last_message_id', lastMessageId.toString());
    }

    const requestOptions = new RequestOptions();
    requestOptions.search = params;
    requestOptions.headers = this.generateHeaders(new Headers());

    return this.http.get(
      `${GlobalService.liveChatAPIPath}visitors/${this.globalService.getClientId()}/chat/get_pending_messages`,
      requestOptions
    )
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

  public sendMessage(message: string) {
    const data = {
      licence_id: GlobalService.liveChatLicenseId,
      secured_session_id: this.secured_session_id,
      message: message
    };
    const requestOptions = new RequestOptions();
    requestOptions.headers = this.generateHeaders();

    return this.http.post(
      `${GlobalService.liveChatAPIPath}visitors/${this.globalService.getClientId()}/chat/send_message`,
      data,
      requestOptions
    )
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

}
