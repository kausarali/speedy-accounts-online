import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { HttpClientService } from './http-client.service';

@Injectable()
export class UserService {

  constructor(
    private globalService: GlobalService,
    private http: HttpClientService
  ) { }

  createUser(data: any) {
    return this.http.post(`${GlobalService.SAOAPIPath}users`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

  makeOrder(data: any) {
    return this.http.post(`${GlobalService.SAOAPIPath}purchases`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

  chargeCard(data: any) {
    return this.http.post(`${GlobalService.SAOAPIPath}charge-card`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

  markAsPaid(data: any) {
    return this.http.post(`${GlobalService.SAOAPIPath}mark-as-paid`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }

  sendContactEmail(data: any) {
    return this.http.post(`${GlobalService.SAOAPIPath}send-contact-email`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError);
  }
}
