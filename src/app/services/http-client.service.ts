import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './global.service';

@Injectable()
export class HttpClientService {

  constructor(
    private globalServce: GlobalService,
    private http: Http
  ) {}

  createAuthorizationHeaders(headers: Headers) {
    headers.append('Authorization', `Bearer ${this.globalServce.token}`);
    return headers;
  }

  get(url: string) {
    const requestOptions: RequestOptions = new RequestOptions();
    const headers: Headers = new Headers();
    requestOptions.headers = this.createAuthorizationHeaders(headers);
    return this.http.get(url, requestOptions);
  }

  post(url: string, data: any) {
    const requestOptions: RequestOptions = new RequestOptions();
    const headers: Headers = new Headers();
    requestOptions.headers = this.createAuthorizationHeaders(headers);
    return this.http.post(url, data, requestOptions);
  }
}
