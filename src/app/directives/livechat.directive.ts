import { Directive, OnInit, Input } from '@angular/core';

@Directive({
  selector: '[appLivechat]'
})
export class LivechatDirective implements OnInit {
  @Input() public licenseId: number;

  ngOnInit(): void {
    this._injectScript();
  }

  constructor() { }

  private _injectScript(): void {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.innerHTML = `
      window.__lc = window.__lc || {};
      window.__lc.license = ${this.licenseId};
      (function() {
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
      })();
    `;
    document.getElementsByTagName('head').item(0).appendChild(script);
  }

}
