export class Option {
  title: string;
  key: string;
  selected: boolean;
  icon: string;
}
