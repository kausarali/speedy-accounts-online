export class Testimonial {
  image: string;
  rating: number;
  text: string;
  position: string;
  name: string;
}
