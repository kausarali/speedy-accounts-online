export class LiveChatMessage {
  type: string;
  message_id: number;
  text: string;
  timestamp: number;
  user_type: string;
}
