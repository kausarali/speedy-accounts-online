import { Option } from './option';

export class Question {
  title: string;
  key: string;
  options: Array<Option>;
}
