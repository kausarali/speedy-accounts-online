export class AgentDetails {
  name: string = 'Agent';
  job_title: string;
  avatar: string;
}
