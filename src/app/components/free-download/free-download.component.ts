import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../../services/http-client.service';
import { GlobalService } from '../../services/global.service';
import { ModalMessageService } from '../../services/modal-message.service';

@Component({
  selector: 'app-free-download',
  templateUrl: './free-download.component.html',
  styleUrls: ['./free-download.component.scss']
})
export class FreeDownloadComponent implements OnInit {
  thankYouContent: boolean = false;
  formData: any = {
    companyName: { value: '', error: false },
    email: { value: '', error: false }
  };
  enterButtonText: string = 'Enter';

  constructor(
    private modalMessageService: ModalMessageService,
    private globalService: GlobalService,
    private http: HttpClientService
  ) { }

  ngOnInit() {
  }

  onEnter() {
    this.enterButtonText = 'Please wait...';
    this.http.post(`${GlobalService.SAOAPIPath}free-download-mail`, {
      company_name: this.formData.companyName.value,
      email: this.formData.email.value
    })
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError)
      .subscribe((res) => {
        this.enterButtonText = 'Enter';
        if (res.errors) {
          this.modalMessageService.error(res.messages[0]);
        } else {
          this.thankYouContent = true;
        }
      }, (err) => {
        this.enterButtonText = 'Enter';
        console.log(err);
      });
  }

  onDownloadNow() {
    // tslint:disable-next-line:max-line-length
    location.href = 'https://speedyaccountsonline.co.uk/blog/wp-content/uploads/2017/10/31-point-checklist-for-preparing-your-own-accounts.pdf';
  }

  shareOnFacebook() {
    const wn = window.open('https://www.facebook.com/sharer/sharer.php?u=https://goo.gl/cmdZmx', '_blank');
    wn.focus();
  }

  shareOnTwitter() {
    // tslint:disable-next-line:max-line-length
    const wn = window.open('https://twitter.com/home?status=Check%20out%2031.%20points%20checklist%20at%20SpeedyAccountsOnline.co.uk%20https%3A//goo.gl/cmdZmx', '_blank');
    wn.focus();
  }

  shareOnGooglePlus() {
    const wn = window.open('https://plus.google.com/share?url=https%3A//goo.gl/cmdZmx', '_blank');
    wn.focus();
  }

  shareOnLinkedIn() {
    // tslint:disable-next-line:max-line-length
    const wn = window.open('https://www.linkedin.com/shareArticle?mini=true&url=https%3A//goo.gl/cmdZmx&title=31.%20points%20checklist&summary=&source=', '_blank');
    wn.focus();
  }
}
