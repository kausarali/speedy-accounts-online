import { Component, OnInit, ViewChildren, AfterViewInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { CompanyService } from '../../services/company.service';
import { Router } from '@angular/router';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { ToastService } from '../../services/toast.service';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  animations: [
    trigger('flyIn', [
      // state('in', style({ opacity: 1 })),
      transition('void => *', [
        style({ opacity: 0, transform: 'translateX(-100%)' }),
        animate('400ms ease-in-out')
      ]),
      transition('* => void', [
        animate('400ms ease-in-out', style({ opacity: 1, transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class LandingPageComponent implements OnInit, AfterViewInit {
  companies: Array<any> = [];
  @ViewChildren('companyName') companyName;
  continueButtonText: string = 'Continue';
  errorMessages: any = {
    noResults: 'Company name not recognised… Please check your entry and try again.',
    noInput: 'Please enter your company name'
  };
  errorMessage: string = '';

  constructor(
    private toastService: ToastService,
    private router: Router,
    private companyService: CompanyService
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.companyName.first.nativeElement.focus();
  }

  public onCompanySearch(companyName: string) {
    if (companyName === '') {
      this.errorMessage = this.errorMessages.noInput;
      return;
    }
    this.continueButtonText = 'Searching...';
    this.companyService.searchCompany(companyName)
      .subscribe((res) => {
        console.log(res);

        if (res.items.length <= 0) {
          this.errorMessage = this.errorMessages.noResults;
          // this.toastService.error('Your search returned no results. Please check your input and try again.');
        }

        this.companies = res.items;
        this.continueButtonText = 'Continue';
      });
  }

  public onCompanySelected(companyNumber: number) {
    this.router.navigate(['/company', companyNumber]);
  }

}
