import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStoreService } from '../../services/data-store.service';
import { ToastService } from '../../services/toast.service';
import { ModalMessageService } from '../../services/modal-message.service';

@Component({
  selector: 'app-company-requirements',
  templateUrl: './company-requirements.component.html',
  styleUrls: ['./company-requirements.component.scss']
})
export class CompanyRequirementsComponent implements OnInit {
  packageDetails: any;

  constructor(
    private modalMessageService: ModalMessageService,
    private toastService: ToastService,
    private dataStore: DataStoreService,
    private router: Router
  ) { }

  ngOnInit() {
    this.packageDetails = this.dataStore.getPackageDetails();
  }

  onContinue() {
    const packageDetails = this.dataStore.getPackageDetails();

    if (packageDetails['actively_trading'].options['dormant'].selected) {
      this.router.navigate(['/instant-quote']);
      return true;
    }

    // tslint:disable-next-line:forin
    for (const packageKey in packageDetails) {
      console.log(packageDetails[packageKey].answered);
      if (!packageDetails[packageKey].answered) {
        // this.toastService.error('Please answer all questions.');
        this.modalMessageService.error('Please answer all the questions.');
        return false;
      }
    }
    this.router.navigate(['/instant-quote']);
    return true;
  }

}
