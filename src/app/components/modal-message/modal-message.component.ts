import { Component, OnInit } from '@angular/core';
import { ModalMessageService } from '../../services/modal-message.service';

@Component({
  selector: 'app-modal-message',
  templateUrl: './modal-message.component.html',
  styleUrls: ['./modal-message.component.scss']
})
export class ModalMessageComponent implements OnInit {
  modal: any = {
    title: 'Error',
    message: 'An error occured.',
    visible: false
  };

  constructor(
    private modalMessageService: ModalMessageService
  ) {}

  ngOnInit() {
    this.modalMessageService.getPubSubber().subscribe((data) => {
      console.log(data);
      this.modal = data;
    });
  }

  onCloseModal() {
    this.modalMessageService.hide();
  }
}
