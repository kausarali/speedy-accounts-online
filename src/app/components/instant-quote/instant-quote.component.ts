import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStoreService } from '../../services/data-store.service';
import { ModalWindowService } from '../../services/modal-window.service';

@Component({
  selector: 'app-instant-quote',
  templateUrl: './instant-quote.component.html',
  styleUrls: ['./instant-quote.component.scss']
})
export class InstantQuoteComponent implements OnInit {
  items: Array<string> = [];
  itemsFree: Array<string> = [];
  servicesToRemove: Array<any> = [];
  packageDetails: any = {};
  totalPrice: any = 0;
  companyInfo: any;

  constructor(
    private modalService: ModalWindowService,
    private dataStore: DataStoreService,
    private router: Router
  ) { }

  ngOnInit() {
    const includedServiceIds = this.dataStore.getServicesIncludedIds();

    if (includedServiceIds.length < 1) {
      this.router.navigate(['/']);
    }

    this.itemsFree = [
      '1x official printed and bound copy of abbreviated accounts',
      '1x confirmation statement filed for you [Call in to redeem - Save up to &pound;100]',
      '1x official printed and bound copy of full accounts',
      'Templates to prepare your own dividend vouchers [Save up to &pound;450]',
      '1x official printed and bound copy of Corporation tax return [CT600]',
      'Unlimited email and phone support',
      '1x official printed and bound copy of Corporation Tax Computation',
      'FREE Lifetime Support'
    ];
    this.packageDetails = this.dataStore.getPackageDetails();
    this.items = this.generateItems(this.packageDetails);
    this.totalPrice = this.dataStore.getTotalPrice();

    this.dataStore.getPubSubber().subscribe((data) => {
      if ('packageDetails' in data) {
        this.packageDetails = data.packageDetails;
        this.items = this.generateItems(this.packageDetails);
        this.totalPrice = this.dataStore.getTotalPrice();
      }
    });

    this.companyInfo = this.dataStore.getCompanyInfo();
  }

  onContinue() {
    this.router.navigate(['/review-and-payment']);
  }

  goToRequirements() {
    this.router.navigate(['/company-requirements']);
  }

  onEmailQuote() {
    this.modalService.toggleEmailQuoteModal(true);
  }

  generateItems(packageDetails: Array<any>) {
    const items = [];
    items.push('Electronic Set of Abbreviated Accounts and Full Accounts');
    if (packageDetails['bookkeeping'].options['yes'].selected) {
      items.push('Bookkeeping up to 300 transactions');
    }
    items.push('Electronic Corporation Tax Return [CT600] and Tax Computation');
    if (packageDetails['employer'].options['notFiled'].selected) {
      items.push( '12 Months of Payroll Submissions');
    }
    items.push('Detailed Breakdown of all Calculations for any Surprise Investigations');
    if (packageDetails['vat'].options['notFiled'].selected) {
      items.push( '12 Months of VAT Submissions');
    }
    return items;
  }
}
