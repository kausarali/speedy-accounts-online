import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewAndPaymentComponent } from './review-and-payment.component';

describe('ReviewAndPaymentComponent', () => {
  let component: ReviewAndPaymentComponent;
  let fixture: ComponentFixture<ReviewAndPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewAndPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewAndPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
