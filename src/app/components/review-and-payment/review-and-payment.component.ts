import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { DataStoreService } from '../../services/data-store.service';
import { ModalWindowService } from '../../services/modal-window.service';
import { ToastService } from '../../services/toast.service';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';
import { ModalMessageService } from '../../services/modal-message.service';

@Component({
  selector: 'app-review-and-payment',
  templateUrl: './review-and-payment.component.html',
  styleUrls: ['./review-and-payment.component.scss']
})
export class ReviewAndPaymentComponent implements OnInit {
  totalPrice: number;
  paymentMethod: string = 'card';
  customerInfo = {
    firstName: {
      value: '',
      error: false,
    },
    lastName: {
      value: '',
      error: false,
    },
    email: {
      value: '',
      error: false,
    },
    tac: {
      value: false,
      error: false,
    },
    marketingEmails: {
      value: true,
      error: false,
    },
    address: {
      value: '',
      error: false,
    },
    city: {
      value: '',
      error: false,
    },
    postCode: {
      value: '',
      error: false,
    },
    phoneNumber: {
      value: '',
      error: false,
    }
  };
  cardInfo = {
    number: '',
    cvv: '',
    expiration_month: '',
    expiration_year: ''
  };
  // stripePKey = 'pk_test_S1C57J6PCCRubYb3IAOhMgOZ';
  stripePKey = 'pk_live_jCsMYbe8cCMJFxdXSoUesuQG';
  purchaseId: string;
  stripeTok: string;
  payPalInit = false;

  constructor(
    private modalMessageService: ModalMessageService,
    private zone: NgZone,
    private globalService: GlobalService,
    private userService: UserService,
    // private toastService: ToastService,
    private modalService: ModalWindowService,
    private router: Router,
    private dataStore: DataStoreService
  ) { }

  ngOnInit() {
    this.injectStripeScript();
    this.injectPayPalScript();
    this.totalPrice = this.dataStore.getTotalPrice();
    this.autoFillInfo();
  }

  autoFillInfo() {
    const personsWithSignificantControl = this.dataStore.getPersonsWithSignificantControl();
    if (personsWithSignificantControl.length > 0) {
      console.log('=== persons with significant controll ===');
      this.customerInfo.firstName.value = personsWithSignificantControl[0].name_elements.forename;
      this.customerInfo.lastName.value = personsWithSignificantControl[0].name_elements.surname;
    }
  }

  /**
   * Stripe and PayPal setup
   */
  injectStripeScript() {
    const stripeScript = document.createElement('script');
    stripeScript.type = 'text/javascript';
    stripeScript.src = 'https://js.stripe.com/v2/';
    // const script2 = document.createElement('script');
    // script2.type = 'text/javascript';
    // script2.innerHTML = `Stripe.setPublishableKey('${this.stripePKey}');`;
    document.getElementsByTagName('head').item(0).appendChild(stripeScript);
    // document.getElementsByTagName('head').item(0).appendChild(script2);
  }

  injectPayPalScript() {
    const payPalScript = document.createElement('script');
    payPalScript.src = 'https://www.paypalobjects.com/api/checkout.js';
    document.getElementsByTagName('head').item(0).appendChild(payPalScript);
  }

  payPalScriptInit() {
    console.log('PayPalScriptInit():');
    const totalAmount: string = String(Number(Math.round(this.totalPrice * 100) / 100).toFixed(2));
    console.log(totalAmount);
    console.log((<any>window).paypal);

    (<any>window).paypal.Button.render({
      env: 'production',             // 'production', 'sandbox'
      client: {
        // SpeedyAccoutsOnline.co.uk
        sandbox: 'AayaYAO2QXOCZZyH_GNBtRAtzCWRBEpqs-iEO2luSIwx-Dug_kPCJPxSawm-0KzkjfhrOy6Gwm4ud2hq',
        production: 'AVxM1t3j2bPc6DWAUz9CTtGOPNivVN6pIfMKaF72TMlObcPVVSnDKpTne1K9h1IsFTbWJdBi3WAjRELu'
        // Test
        // sandbox: 'AeAaq0JXuFg8_0gvFrGrA_KjBDWRl4WfiKhAfEXf8oeBUzXkmdhyFgaFpOGbqK5Zh31hWcydRQOTMBIW',
        // production: 'AdAof63mUtZuxjsx33FglA-ywOWoPJK10RIWx72RwVv0Xg8wb4rhX1wHLKAPizp2c6u70f1l38EMrn4r'
      },
      commit: false,        // Show a 'Pay Now' button
      style: {
        size: 'medium',     // 'small', 'medium', 'responsive'
        color: 'blue',      // 'gold', 'blue', 'silver', 'black'
        shape: 'pill',      // 'pill', 'rect'
        label: 'checkout'
      },
      payment: (data, actions) => {
        // setup the payment here
        return actions.payment.create({
          payment: {
            transactions: [
              {
                amount: {
                  total: totalAmount,
                  currency: 'GBP'
                }
              }
            ]
          },
          experience: {
            input_fields: {
              no_shipping: 1
            }
          }
        });
      },
      onAuthorize: (data, actions) => {
        // execute payment here
        return actions.payment.execute().then((payment) => {
          console.log('PAYMENT EXECUTED:');
          console.log(payment);
          // this.toastService.success('Payment successfull.');
          this.dataStore.setPaid(true);
          this.createUserAccount()
            .then(this.addOrderToDatabase.bind(this))
            .then(purchaseId => {
              return new Promise((resolve, reject) => {
                this.markAsPaid({
                  purchaseId,
                  chargeInfo: JSON.stringify(payment)
                }).then(() => {
                  resolve();
                });
              });
            })
            .then(() => {
              this.router.navigate(['/thank-you']);
            })
            .catch(err => console.log(err));
        });
      }
    }, '#paypal-button');
  }

  /**
   * Validation
   */
  validateInputs() {
    const errors: Array<string> = [];

    // reset errors
    for (const key in this.customerInfo) {
      if (this.customerInfo[key]) {
        // console.log('Check:');
        // console.log(this.customerInfo[key]);
        this.customerInfo[key].error = false;
      }
    }

    // console.log('Errors reseted:');
    // console.log(this.customerInfo);


    if (this.customerInfo.firstName.value === '') {
      errors.push('First name is required.');
      this.customerInfo.firstName.error = true;
    }

    if (this.customerInfo.lastName.value === '') {
      errors.push('Last name is required.');
      this.customerInfo.lastName.error = true;
    }

    if (this.customerInfo.email.value === '') {
      errors.push('E-mail is required.');
      this.customerInfo.email.error = true;
    }

    if (this.customerInfo.phoneNumber.value === '') {
      errors.push('Telephone number is required.');
      this.customerInfo.phoneNumber.error = true;
    }

    if (this.customerInfo.address.value === '') {
      errors.push('Address is required.');
      this.customerInfo.address.error = true;
    }

    if (this.customerInfo.city.value === '') {
      errors.push('City is required.');
      this.customerInfo.city.error = true;
    }

    if (this.customerInfo.postCode.value === '') {
      errors.push('Post code is required.');
      this.customerInfo.postCode.error = true;
    }

    // console.log('Validation finished:');
    // console.log(this.customerInfo);


    if (errors.length > 0) {
      //   this.toastService.error(errors[0]);
      // ToDo: Fix new lines
      this.modalMessageService.error(errors[0]);
      return false;
    }

    return true;
  }

  validateCardInfo() {
    const errors: Array<string> = [];

    if (!this.cardInfo.number || this.cardInfo.number === '') {
      errors.push('Card number is required.');
    }
    if (!this.cardInfo.cvv || this.cardInfo.cvv === '') {
      errors.push('Card CVV is required.');
    }
    if (!this.cardInfo.expiration_month || this.cardInfo.expiration_month === '') {
      errors.push('Card expiration month is required.');
    }
    if (!this.cardInfo.expiration_year || this.cardInfo.expiration_year === '') {
      errors.push('Card expiration year is required.');
    }

    if (errors.length > 0) {
      this.modalMessageService.error(errors[0]);
      return false;
    }

    return true;
  }

  validateTOS() {
    const errors: Array<string> = [];

    if (!this.customerInfo.tac.value) {
      errors.push('You have to agree to terms and conditions.');
    }

    if (errors.length > 0) {
      // this.toastService.error(errors[0]);
      this.modalMessageService.error(errors[0]);
      return false;
    }

    return true;
  }

  /**
   * Stripe flow
   */
  onSubmitOrder() {
    if (!this.validateInputs()) {
      this.modalMessageService.error('Please fill in the above fields.');
      return false;
    }

    if (!this.validateTOS()) {
      this.modalMessageService.error('You have to agree to terms and conditions.');
      return false;
    }

    if (!this.validateCardInfo()) {
      this.modalMessageService.error('Please make sure you entered your card information corectly.');
      return false;
    }

    this.createUserAccount()
      .then(this.addOrderToDatabase.bind(this))
      .then(this.stripeGetToken.bind(this))
      .then(this.chargeCard.bind(this))
      .then(this.markAsPaid.bind(this))
      .then(data => {
        console.log(data);
        this.router.navigate(['/thank-you']);
      })
      .catch(err => console.log(err));
  }

  markAsPaid(data: any) {
    return new Promise((resolve, reject) => {
      this.userService.markAsPaid({
        purchase_id: data.purchaseId,
        payment_info: data.chargeInfo
      })
        .subscribe(res => {
          this.globalService.handleResponse(res);
          resolve();
        }, err => reject(err));
    });
  }

  stripeGetToken(purchaseId: number) {
    console.log('stripeGetToken():');
    return new Promise((resolve, reject) => {
      (<any>window).Stripe.setPublishableKey(this.stripePKey);
      (<any>window).Stripe.card.createToken({
        number: this.cardInfo.number,
        cvc: this.cardInfo.cvv,
        exp_month: this.cardInfo.expiration_month,
        exp_year: this.cardInfo.expiration_year
      }, (status: number, response: any) => {
        this.zone.run(() => {
          if (status !== 200) {
            reject(response);
          }
          // response.id
          resolve({ purchaseId, response });
        });
      });
    });
  }

  chargeCard(data) {
    return new Promise((resolve, reject) => {
      const postData = {
        purchase_id: data.purchaseId,
        tok: data.response.id
      };
      this.userService.chargeCard(postData)
        .subscribe(res => {
          const payload = this.globalService.handleResponse(res);
          console.log(payload);
          // if (res && res.error && res.error.message) {
          // this.toastService.error(res.error.message);
          // this.modalMessageService.error(res.error.message);
          // }
          // console.log('chargeCard():');
          // console.log(res);
          // this.toastService.success(`Charge status: ${res.status}`);
          this.dataStore.setPaid(true);
          resolve({
            purchaseId: data.purchaseId,
            chargeInfo: payload.charge_info
          });
        }, err => reject(err));
    });
  }

  /**
   * PayPal flow
   */
  onPayPalSelected() {

    if (!this.validateInputs()) {
      // this.toastService.warning('Please make sure all the previous fields are filled correctly and try again.');
      this.modalMessageService.error('Please make sure all the previous fields are filled correctly and try again.');
      this.paymentMethod = 'card';
      return false;
    }

    if (!this.payPalInit) {
      this.payPalScriptInit();
      this.payPalInit = true;
    }
  }

  /**
   * Card and PayPal
   */
  createUserAccount() {
    return new Promise((resolve, reject) => {
      this.userService.createUser({
        name: `${this.customerInfo.firstName.value} ${this.customerInfo.lastName.value}`,
        email: this.customerInfo.email.value,
        address: this.customerInfo.address.value,
        city: this.customerInfo.city.value,
        post_code: this.customerInfo.postCode.value,
        phone_number: this.customerInfo.phoneNumber.value,
        marketing_emails: this.customerInfo.marketingEmails.value
      })
        .subscribe((res) => {
          const payload = this.globalService.handleResponse(res);
          if (!res.errors && 'token' in payload) {
            this.globalService.setToken(payload.token);
            resolve();
            // this.status.createUserAccount = true;
            // this.addOrderToDatabase();
          }
        }, (err) => reject(err));
    });
  }

  addOrderToDatabase() {
    return new Promise((resolve, reject) => {
      const data = {
        services: this.dataStore.getServicesIncludedIds()
      };
      this.userService.makeOrder(data)
        .subscribe(res => {
          const payload = this.globalService.handleResponse(res);
          if (res.errors || !payload) { reject(res); }
          resolve(payload.purchase_id);
        }, err => reject(err));
    });
  }

  /**
   * Other methodes
   */
  onTACShow(e) {
    e.preventDefault();
    this.modalService.toggleTermsOfServiceModal(true);
  }
}
