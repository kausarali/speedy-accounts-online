import { Component, OnInit } from '@angular/core';
import { Testimonial } from '../../models/testimonial';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
  testimonial: Testimonial = {
    image: '../../../assets/img/testimonial.png',
    rating: 5,
    text: 'I spent 30 minutes preparing my documents and speedy took care of the rest!<br />I wouldnt know what to do without them.',
    position: 'Director',
    name: 'Breaking Barrierz Limited'
  };

  constructor() { }

  ngOnInit() {
  }

}
