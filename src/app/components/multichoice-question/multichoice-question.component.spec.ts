import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultichoiceQuestionComponent } from './multichoice-question.component';

describe('MultichoiceQuestionComponent', () => {
  let component: MultichoiceQuestionComponent;
  let fixture: ComponentFixture<MultichoiceQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultichoiceQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultichoiceQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
