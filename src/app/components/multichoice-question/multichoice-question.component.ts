import { Component, Input, OnInit } from '@angular/core';
import { Question } from '../../models/question';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-multichoice-question',
  templateUrl: './multichoice-question.component.html',
  styleUrls: ['./multichoice-question.component.scss']
})
export class MultichoiceQuestionComponent implements OnInit {
  packageDetails: any;
  @Input() packageKey: string;
  @Input() enabled: boolean = true;

  constructor(private dataStore: DataStoreService) {
  }

  ngOnInit() {
    this.packageDetails = this.dataStore.getPackageDetails()[this.packageKey];
  }

  public onClick(optionKey: string) {
    for (const key in this.packageDetails.options) {
      if (key === optionKey) {
        // update UI
        this.packageDetails.options[key].selected = true;
      } else {
        this.packageDetails.options[key].selected = false;
      }
    }
    // update dataStore
    this.dataStore.updatePackage(this.packageKey, optionKey);
  }
}
