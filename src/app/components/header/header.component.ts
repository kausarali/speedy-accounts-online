import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuOpened: boolean = false;
  menuIcon: string = './assets/img/menu-icon.svg';
  showStartHere: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.showStartHere = val.url === '/about-us' || val.url === '/contact';
      }
    });
  }

  public onHamburgerMenuClicked() {
    this.menuOpened = !this.menuOpened;
    (this.menuOpened) ? this.menuIcon = './assets/img/x-icon.svg' : this.menuIcon = './assets/img/menu-icon.svg';
  }

}
