import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CompanyService } from '../../services/company.service';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {

  companyNumber: number;
  companyDetails: any = {
    accounts: {
      next_due: ''
    }
  };
  personsWithSignificantControl: Array<any> = [
    {
      name_elements: {
        forename: '',
        lastname: ''
      }
    }
  ];

  constructor(
    private dataStore: DataStoreService,
    private router: Router,
    private companyService: CompanyService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    console.log('ngOnInit():');
    this.activatedRoute.params.subscribe((params: Params) => {
      this.companyNumber = params['company_number'];
      this.dataStore.setCompanyNumber(params['company_number']);
      this.getCompanyProfile(this.companyNumber);
      this.getPersonsWithSignificantControl(this.companyNumber);
    });
  }

  getCompanyProfile(companyNumber: number) {
    this.companyService.getProfile(companyNumber)
      .subscribe((res) => {
        this.companyDetails = res;
        this.dataStore.setCompanyInfo(res);
      });
  }

  getPersonsWithSignificantControl(companyNumber: number) {
    this.companyService.getPersonsWithSignificantControl(companyNumber)
      .subscribe((res) => {
        this.personsWithSignificantControl = res.items;
        this.dataStore.setPersonsWithSignificantControl(res.items);
      });
  }

  onContinue() {
    this.router.navigate(['/company-requirements']);
  }
}
