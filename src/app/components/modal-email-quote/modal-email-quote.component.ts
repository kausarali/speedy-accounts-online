import { Component, OnInit } from '@angular/core';
import { ModalWindowService } from '../../services/modal-window.service';
import { HttpClientService } from '../../services/http-client.service';
import { GlobalService } from '../../services/global.service';
import { DataStoreService } from '../../services/data-store.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-modal-email-quote',
  templateUrl: './modal-email-quote.component.html',
  styleUrls: ['./modal-email-quote.component.scss']
})
export class ModalEmailQuoteComponent implements OnInit {
  emailQuoteModal = { visible: false };
  email: string;

  constructor(
    private toastService: ToastService,
    private dataStore: DataStoreService,
    private globalService: GlobalService,
    private http: HttpClientService,
    private modalService: ModalWindowService
  ) { }

  ngOnInit() {
    this.modalService.getPubSubber().subscribe((data) => {
      if ('emailQuoteModal' in data) {
        this.emailQuoteModal = data.emailQuoteModal;
      }
    });
  }

  onCloseModal() {
    this.modalService.toggleEmailQuoteModal(false);
  }

  onSend() {
    this.modalService.toggleEmailQuoteModal(false);
    this.toastService.wait('Please wait...');
    const data = {
      email: this.email,
      services: this.dataStore.getServicesIncludedIds(),
      company_number: this.dataStore.getCompanyNumber(),
      company_name: this.dataStore.getCompanyInfo().company_name
    };
    this.http.post(`${GlobalService.SAOAPIPath}quotes`, data)
      .map(GlobalService.extractData)
      .catch(GlobalService.handleError)
      .subscribe(res => {
        console.log(res);
        this.toastService.clearAll();
        this.globalService.handleResponse(res);
      });
  }
}
