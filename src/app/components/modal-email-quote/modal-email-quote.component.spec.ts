import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEmailQuoteComponent } from './modal-email-quote.component';

describe('ModalEmailQuoteComponent', () => {
  let component: ModalEmailQuoteComponent;
  let fixture: ComponentFixture<ModalEmailQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEmailQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEmailQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
