import { Component, OnInit, HostListener, Inject, Input } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';
import { DOCUMENT } from '@angular/platform-browser';
import { GlobalService } from '../../services/global.service';
import { ModalWindowService } from '../../services/modal-window.service';
import { FacebookService, InitParams } from 'ngx-facebook';

@Component({
  selector: 'app-quote-sidebar',
  templateUrl: './quote-sidebar.component.html',
  styleUrls: ['./quote-sidebar.component.scss']
})
export class QuoteSidebarComponent implements OnInit {
  @Input() topGap: number = 500;
  @Input() maxTopOffset: number = 1650;
  @Input() showYourPremium: boolean = true;
  @Input() showLiveChat: boolean = true;
  @Input() showTestimonials: boolean = false;
  @Input() showFacebookPageWidget: boolean = false;
  @Input() showServicesIncluded: boolean = true;
  @Input() showGetStarted: boolean = false;
  packageDetails: any = {};
  companyInfo: any = { company_name: '' };
  totalPrice: any = 0;
  fixedSidebarMarginTop: number = 0;
  licenseId: number = GlobalService.liveChatLicenseId;
  servicesIncluded: Array<string> = [];

  constructor(
    private fb: FacebookService,
    private modalService: ModalWindowService,
    private globalService: GlobalService,
    @Inject(DOCUMENT) private document: Document,
    private dataStore: DataStoreService
  ) { }

  ngOnInit() {
    this.packageDetails = this.dataStore.getPackageDetails();
    this.servicesIncluded = this.dataStore.getServicesIncluded();
    this.totalPrice = this.dataStore.getTotalPrice();
    this.dataStore.getPubSubber().subscribe((data) => {
      if ('packageDetails' in data) {
        this.packageDetails = this.dataStore.getPackageDetails();
        this.servicesIncluded = this.dataStore.getServicesIncluded();
        this.totalPrice = this.dataStore.getTotalPrice();
      }
    });
    this.companyInfo = this.dataStore.getCompanyInfo();
    this.loadFacebookSDK(() => {
      this.initFacebookModule();
    });
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const topOffset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    // console.log(topOffset);

    if (window.screen.width < 1000) {
      return;
    }

    if (topOffset > this.maxTopOffset) {
      this.fixedSidebarMarginTop = this.maxTopOffset - this.topGap;
      return;
    }

    if (topOffset > this.topGap) {
      this.fixedSidebarMarginTop = topOffset - this.topGap;
      return;
    }

    this.fixedSidebarMarginTop = 0;
  }

  onEmailQuote() {
    this.modalService.toggleEmailQuoteModal(true);
  }

  loadFacebookSDK(callback) {
    const facebookSDKScript = document.createElement('script');
    facebookSDKScript.type = 'text/javascript';
    facebookSDKScript.src = '//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10';
    document.getElementsByTagName('head').item(0).appendChild(facebookSDKScript);
    facebookSDKScript.onload = function () {
      callback();
    };
  }

  initFacebookModule() {
    this.fb.init({
      appId: '1080608295409052',
      xfbml: true,
      version: 'v2.8'
    });
  }
}
