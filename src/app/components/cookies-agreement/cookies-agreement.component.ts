import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cookies-agreement',
  templateUrl: './cookies-agreement.component.html',
  styleUrls: ['./cookies-agreement.component.scss']
})
export class CookiesAgreementComponent implements OnInit {
  agreedToCookiesPolicy: boolean = false;

  constructor() { }

  ngOnInit() {
    const agreed = localStorage.getItem('agreed_to_cookies_policy');
    if (agreed === 'true') {
      this.agreedToCookiesPolicy = true;
    }
  }

  onAgree() {
    this.agreedToCookiesPolicy = true;
    localStorage.setItem('agreed_to_cookies_policy', 'true');
  }
}
