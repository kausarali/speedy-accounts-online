import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiesAgreementComponent } from './cookies-agreement.component';

describe('CookiesAgreementComponent', () => {
  let component: CookiesAgreementComponent;
  let fixture: ComponentFixture<CookiesAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookiesAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiesAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
