import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../services/data-store.service';

@Component({
  selector: 'app-remove-from-quote',
  templateUrl: './remove-from-quote.component.html',
  styleUrls: ['./remove-from-quote.component.scss']
})
export class RemoveFromQuoteComponent implements OnInit {
  @Input() packageKey: any;
  packageDetails: any;
  expanded: boolean = true;

  constructor(private dataStore: DataStoreService) { }

  ngOnInit() {
    this.packageDetails = this.dataStore.getPackageDetails();

    // this.expanded = this.isOptionSelected(this.packageKey);

    this.dataStore.getPubSubber().subscribe((data) => {
      if ('packageDetails' in data) {
        this.packageDetails = data.packageDetails;
      }
    });
  }


  isOptionSelected(pckgKey: string) {
    // tslint:disable-next-line:forin
    for (const optKey in this.packageDetails[pckgKey].options) {
      if (this.packageDetails[pckgKey].options[optKey].selected
        && this.packageDetails[pckgKey].options[optKey].servicesIncluded.length > 0) {
        return true;
      }
    }
    return false;
  }

  onYesClicked(packageKey: string) {
    this.dataStore.removePackage(packageKey);
    this.expanded = false;
  }

  onNoClicked(packageKey: string) {
    this.dataStore.addPackage(packageKey);
    this.expanded = false;
  }

  onToggleVisibility() {
    this.expanded = !this.expanded;
  }
}
