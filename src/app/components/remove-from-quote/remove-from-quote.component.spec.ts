import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveFromQuoteComponent } from './remove-from-quote.component';

describe('RemoveFromQuoteComponent', () => {
  let component: RemoveFromQuoteComponent;
  let fixture: ComponentFixture<RemoveFromQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveFromQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveFromQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
