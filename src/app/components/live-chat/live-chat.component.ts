import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { LiveChatService } from '../../services/live-chat.service';
import { Observable } from 'rxjs/Observable';
import { AgentDetails } from '../../models/agent-details';
import { LiveChatMessage } from '../../models/live-chat-message';

@Component({
  selector: 'app-live-chat',
  templateUrl: './live-chat.component.html',
  styleUrls: ['./live-chat.component.scss']
})
export class LiveChatComponent implements OnInit, AfterViewChecked {
  agentDetails: AgentDetails = new AgentDetails();
  busy: boolean = false;
  message: string = '';
  messages: Array<LiveChatMessage> = [{
    type: 'message',
    message_id: 0,
    text: 'Need help? Send us a message...',
    timestamp: 0,
    user_type: 'agent'
  }];
  lastMessageId: number;
  @ViewChild('messagesContainer') private messagesContainer: ElementRef;
  chatStarted: boolean = false;

  constructor(
    private liveChatService: LiveChatService
  ) {
    console.log('LiveChatComponent: constructor():');
  }

  ngOnInit() { }

  public startChat() {

    if (this.chatStarted) {
      return;
    }

    this.messages = [{
      type: 'message',
      message_id: 0,
      text: 'We are working on connecting you to the first available agent. Please wait...',
      timestamp: 0,
      user_type: 'agent'
    }];

    this.liveChatService.startChat(startedChat => {
      if (startedChat) {
        console.log('LiveChatComponent: startChat(): startedChat');
        this.startListeningForMessages();
        this.chatStarted = true;
      }
      if (!startedChat) {
        console.log('LiveChatComponent: startChat(): !startedChat');
        alert('Failed to start the chat!');
      }
    });
  }

  public startListeningForMessages() {
    console.log('LiveChatComponent: startListeningForMessages():');
    Observable.interval(1000)
      .takeWhile(() => !false)
      .subscribe(i => {
        if (!this.busy) {
          this.busy = true;
          this.liveChatService.getMessages(this.lastMessageId)
            .subscribe(data => {
              console.log(data);
              this.busy = false;
              if ('events' in data) {
                this.processEvents(data.events);
              }
            }, err => console.log(err));
        }
      });
  }

  public scrollMessagesToTheBottom() {
    try {
      setTimeout(() => {
        this.messagesContainer.nativeElement.scrollTop = this.messagesContainer.nativeElement.scrollHeight;
      }, 100);
    } finally { }
  }

  ngAfterViewChecked() {
    // this.scrollMessagesToTheBottom();
  }

  public processEvents(events: Array<any>) {
    events.forEach((event, index) => {
      if ('type' in event) {
        if (event.type === 'agent_details') {
          this.agentDetails = event.agent;
        }
        if (event.type === 'message') {
          this.messages.push(event);
          this.scrollMessagesToTheBottom();
        }
      }
    });
    if (this.messages.length > 0) {
      const amountOfMessages = this.messages.length;
      const lastMessage: LiveChatMessage = this.messages[amountOfMessages - 1];
      if ('message_id' in lastMessage) {
        this.lastMessageId = lastMessage.message_id;
      }
    }
  }

  public onSendMessage(message: string) {
    this.liveChatService.sendMessage(this.message)
      .subscribe(res => {
        console.log(res);
      });
    this.message = '';
  }
}
