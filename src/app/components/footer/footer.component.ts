import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() showLinkedIn: boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.showLinkedIn) {
      // this.injectLinkedInButton();
    }
  }

  injectLinkedInButton() {
    const linkedInScript = document.createElement('script');
    linkedInScript.type = 'text/javascript';
    linkedInScript.src = 'http://platform.linkedin.com/in.js';
    const linkedInScript2 = document.createElement('script');
    linkedInScript2.type = 'in/recommendproduct';
    linkedInScript2['data-company'] = '13434601';
    // ToDo: Add product ID
    linkedInScript2['data-product'] = '';
    linkedInScript2['data-counter'] = 'right';
    document.getElementById('linked-in-recommend').appendChild(linkedInScript);
    document.getElementById('linked-in-recommend').appendChild(linkedInScript2);
  }
}
