import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactDetails: any = {
    firstName: { value: '', errors: false },
    lastName: { value: '', errors: false },
    email: { value: '', errors: false },
    phoneNumber: { value: '', errors: false },
    message: { value: '', errors: false }
  };
  sendMessageButtonText: string = 'Submit';

  constructor(
    private globalService: GlobalService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  onSendMessage() {
    this.sendMessageButtonText = 'Sending...';
    this.userService.sendContactEmail({
      first_name: this.contactDetails.firstName.value,
      last_name: this.contactDetails.lastName.value,
      email: this.contactDetails.email.value,
      phone_number: this.contactDetails.phoneNumber.value,
      message: this.contactDetails.message.value
    })
      .subscribe((res) => {
        this.globalService.handleResponse(res);

        if (!res.errors) {
          this.sendMessageButtonText = 'Message sent!';
          this.clearForm();
        } else {
          this.sendMessageButtonText = 'Failed to send message.';
        }

        setTimeout(() => {
          this.sendMessageButtonText = 'Submit';
        }, 3000);
      }, (err) => {
        this.sendMessageButtonText = 'Submit';
      });
  }

  clearForm() {
    for (const key in this.contactDetails) {
      if (this.contactDetails[key] && this.contactDetails[key].value) {
        this.contactDetails[key].value = '';
      }
    }
  }
}
