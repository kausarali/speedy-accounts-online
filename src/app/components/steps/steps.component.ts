import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStoreService } from '../../services/data-store.service';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent implements OnInit {
  @Input() currentStep: number = 1;
  companyNumber: any;

  constructor(
    private toastService: ToastService,
    private dataStore: DataStoreService,
    private router: Router
  ) { }

  ngOnInit() {
    this.companyNumber = this.dataStore.getCompanyNumber();
  }

  goToStep(step: number) {

    switch (step) {
      case 1:
        this.router.navigate(['/company', this.companyNumber]);
        break;
      case 2:
        this.router.navigate(['/company-requirements']);
        break;
      case 3:
        this.router.navigate(['/instant-quote']);
        break;
      case 4:
        this.router.navigate(['/review-and-payment']);
        break;
      case 5:
        this.router.navigate(['/thank-you']);
        break;

      default:
        console.log('Unknown step.');
        break;
    }

  }
}
