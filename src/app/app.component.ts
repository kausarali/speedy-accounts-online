import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ModalWindowService } from './services/modal-window.service';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

declare var ga: Function;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('modalFadeIn', [
      state('invisible', style({
        opacity: 0,
        display: 'none'
      })),
      state('visible', style({
        opacity: 1,
        display: 'fixed'
      })),
      transition('invisible => visible', animate('250ms ease-in')),
      transition('visible => invisible', animate('250ms ease-out'))
    ])
  ]
})
export class AppComponent implements OnInit {
  termsOfServiceModal = {
    visible: false
  };

  constructor(
    private modalService: ModalWindowService,
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    this.router.events.distinctUntilChanged((previous: any, current: any) => {
      if (current instanceof NavigationEnd) {
        return previous.url === current.url;
      }
      return true;
    }).subscribe((data: any) => {
      ga('set', 'page', data.url);
      ga('send', 'pageview');
    });

    this.modalService.getPubSubber().subscribe((res) => {
      if ('termsOfServiceModal' in res) {
        this.termsOfServiceModal = res.termsOfServiceModal;
      }
    });

  }
}
